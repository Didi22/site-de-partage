-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 22 mars 2021 à 14:05
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sitepartage`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `categorie` enum('livre','bricolage','electromenager','vetement','aide','jeux','sport','musique','autre') COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`id`, `categorie`, `nom`, `description`, `url`) VALUES
(2, 'livre', '1984', 'très ancien', 'https://images.app.goo.gl/MRY9vzNqE8uXdkaS7'),
(3, 'electromenager', 'Téléviseur', 'Full HD', 'https://images.app.goo.gl/tWDzXReRer48Qazw7'),
(4, 'livre', 'Harry Potter', 'bon état', 'https://images.app.goo.gl/NRvxQAmB2NGVaPgJ9'),
(5, 'livre', 'Death Note tome 1', 'neuf', 'https://images.app.goo.gl/DpNuZHMw9FxnirUR8'),
(7, 'vetement', 'Robe', 'saison été', 'https://images.app.goo.gl/E79oYwuwgYCJDcYK6'),
(9, 'autre', 'Monopoly', 'ancien et en franc', 'https://images.app.goo.gl/Cfc7dUFPauLRqwcz6'),
(11, 'vetement', 'T-Shirt', 'femme motif Mickey', 'https://images.app.goo.gl/eBfFWsUgAHPJoJWS6'),
(12, 'bricolage', 'tondeuse', 'thermique', 'https://images.app.goo.gl/5CN8wRk2TeqHGuiSA'),
(13, 'vetement', 'Pantalon', 'Sarouel femme', 'https://images.app.goo.gl/V7UraKTEMAGToL7G8'),
(14, 'bricolage', 'tournevis', 'sonique', 'https://images.app.goo.gl/fiFircjynUy7GXvZ8');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
