<?php

use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Models\Article;
use App\Models\Livre;
use App\Models\Image;
use App\Models\Possede;
use App\Models\Vetement;
use App\Models\Ami;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\LivreController;
use App\Http\Controllers\PossedeController;
use App\Http\Controllers\VetementController;
use App\Http\Controllers\AmiController;
use App\Http\Controllers\ImageController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewFriend;
use Illuminate\Notifications\Notifiable;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome'); });

// Accueil
Route::get('/dashboard', function () { 
    $user = Auth::user()->id; 
    $images = Image::all();
    $amis = Ami::join('users', "amis.ami_id", "=", "users.id")->where('statut', '=', 'ami')->where('user_id', '=', $user)->get();
    $recherche = array();
    
    $objetsArray = array();
    $objetsArray = Article::join("possedes", "articles.id", "=", "possedes.article_id")->orderBy('articles.id', 'desc')->limit('4')->get();
    
    return view('dashboard', [
        'user' => $user,
        'amis' => $amis,
        'images' => $images,
        'objets' => $objetsArray
    ]); 

})->middleware(['auth'])->name('dashboard');

Route::post('/dashboard', function () {
    $user = Auth::user()->id;
    $images = Image::all();
    $amis = Ami::join('users', "amis.ami_id", "=", "users.id")->where('statut', '=', 'ami')->where('user_id', '=', $user)->get();
    $recherche = array();

    $request = request()->input('rechercheArticle');
    $requestLivre = request()->input('rechercheLivre');
    $requestVetement = request()->input('rechercheVetement');
    foreach($amis as $ami){
        if($request != null){
            $recherche = Article::join("possedes", "articles.id", "=", "possedes.article_id")->where('nom', 'LIKE', '%' . $request . '%')->orwhere('description', 'LIKE', '%' . $request . '%')->orwhere('categorie', 'LIKE', '%' . $request . '%')->get();  
        }
        if($requestLivre != null){
            $recherche = Article::join("possedes", "articles.id", "=", "possedes.article_id")->join("livres", "livres.article_id", "=", "articles.id")->where('nom', 'LIKE', '%' . $requestLivre . '%')->orwhere('description', 'LIKE', '%' . $requestLivre . '%')->orwhere('categorie', 'LIKE', '%' . $requestLivre . '%')->orwhere('auteur', 'LIKE', '%' . $requestLivre . '%')->orwhere('edition', 'LIKE', '%' . $requestLivre . '%')->orwhere('ISBN', 'LIKE', '%' . $requestLivre . '%')->orwhere('type_de_support', 'LIKE', '%' . $requestLivre . '%')->orwhere('date_de_publication', 'LIKE', '%' . $requestLivre . '%')->get();
        }
        if($requestVetement != null){
            $recherche = Article::join("possedes", "articles.id", "=", "possedes.article_id")->join("vetements", "vetements.article_id", "=", "articles.id")->where('nom', 'LIKE', '%' . $requestVetement . '%')->orwhere('description', 'LIKE', '%' . $requestVetement . '%')->orwhere('categorie', 'LIKE', '%' . $requestVetement . '%')->orwhere('taille', 'LIKE', '%' . $requestVetement . '%')->get();
        }
    } 

    $objetsArray = array();
    $objetsArray = Article::join("possedes", "articles.id", "=", "possedes.article_id")->orderBy('articles.id', 'desc')->limit('4')->get(); 

    return view('/dashboard', [
        'user' => $user,
        'amis' => $amis,
        'images' => $images,
        'objets' => $objetsArray,
        'recherche' => $recherche
    ]);
});

//Footer
Route::get('/confidentialite', function (){ return view('confidentialite'); });

Route::get('/cookies', function (){ return view('cookies'); });

//Articles
Route::get('/articles', [ArticleController::class, 'index'])->middleware(['auth'])->name('articles');

Route::get('/article/ajout', [ArticleController::class, 'create'])->middleware(['auth'])->name('ajoutarticle');

Route::post('/article/nouvel', [ArticleController::class, 'store'])->middleware(['auth'])->name('nouvelarticle');

Route::get('/article/update/{id}', [ArticleController::class, 'edit'])->middleware(['auth']);

Route::patch('/article/update/{id}', [ArticleController::class, 'update'])->middleware(['auth']);

Route::get('/article/delete/{id}', [ArticleController::class, 'show'])->middleware(['auth'])->name('deletearticle');

Route::delete('/article/delete/{id}', [ArticleController::class, 'destroy'])->middleware(['auth'])->name('deletearticle');

// Bibliotheque
Route::get('/bibliotheque', [LivreController::class, 'index'])->middleware(['auth'])->name('biblio');

Route::get('/livre/ajout', [LivreController::class, 'create'])->middleware(['auth'])->name('ajoutlivre');

Route::post('/livre/nouveau', [LivreController::class, 'store'])->middleware(['auth'])->name('nouveaulivre');

Route::get('/livre/update/{id}', [LivreController::class, 'edit'])->middleware(['auth']);

Route::patch('/livre/update/{id}', [LivreController::class, 'update'])->middleware(['auth']);

Route::get('/livre/delete/{id}', [LivreController::class, 'show'])->middleware(['auth']);

Route::delete('/livre/delete/{id}', [LivreController::class, 'destroy'])->middleware(['auth']);

// Dressing
Route::get('/dressing', [VetementController::class, 'index'])->middleware(['auth'])->name('dressing');

Route::get('/vetement/ajout', [VetementController::class, 'create'])->middleware(['auth'])->name('ajoutvetement');

Route::post('/vetement/nouveau', [VetementController::class, 'store'])->middleware(['auth'])->name('nouveauvetement');

Route::get('/vetement/update/{id}', [VetementController::class, 'edit'])->middleware(['auth']);

Route::patch('/vetement/update/{id}', [VetementController::class, 'update'])->middleware(['auth']);

Route::get('/vetement/delete/{id}', [VetementController::class, 'show'])->middleware(['auth']);

Route::delete('/vetement/delete/{id}', [VetementController::class, 'destroy'])->middleware(['auth']);

// Systeme amitie
Route::get('/amis', [AmiController::class, 'index'])->middleware(['auth'])->name('amis');

Route::get('/ami/recherche', function (){ 
    $user = Auth::user()->id; 
    
    return view('newami'); 
})->middleware(['auth'])->name('rechercheami');

Route::post('/ami/recherche',[AmiController::class, 'create'])->middleware(['auth']);

Route::get('/ami/ajout', [AmiController::class, 'show'])->middleware(['auth'])->name('ajoutami');

Route::post('/ami/ajout', [AmiController::class, 'edit'])->middleware(['auth']);

// Sauvegarde images
Route::resource('images', ImageController::class)->only('index', 'store');

// Authentification
require __DIR__.'/auth.php';