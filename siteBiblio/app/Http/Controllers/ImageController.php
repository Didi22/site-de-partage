<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::all();
        return view('images.index', compact('images'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validation du formulaire à l'aide des règles spécifiques aux type
        // de données fichier.
        $request->validate([
            'image' => 'required|image|max:1024'    // type image, poids max de 1024ko
                                                    // voir autres règles file,mimes,mimetypes,...
        ]);

        // Laravel transforme le champ `image` du formulaire en un objet `File`.
        // On utilise la méthode store de cet objet pour sauvegarder le fichier dans
        // le disque public dossier uploads. La méthode retourne le chemin du fichier sauvegardé.
        $path = $request->image->store('public/uploads');

        // On utilise ma méthode Storage::url avec le chemin pour récupérer l'adresse publique
        // du fichier. C'est cette adresse que l'on stocke dans la BDD.
        $url = Storage::url($path);

        // Enregistrement dans la BDD, on alimente la colonne url avec
        // l'adresse public de l'image fraîchement sauvegardée.
        Image::create(['url' => $url]);

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        //
    }
}
