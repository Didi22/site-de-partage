<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Possede;
use App\Models\Livre;
use App\Models\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class ArticleController extends Controller
{
    /** Affichage des articles de utilisateur
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::all();
        $user = Auth::user()->id;
        $articles = Article::join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', '!=', 'livre')->where('categorie', '!=', 'vetement')->where('user_id', '=', $user)->get();
        
        $ordre = request()->input('ordre', 'nom');
        $objets = array();
        $objetsUser = Possede::join("articles", "possedes.article_id", "articles.id")->where('user_id', '=', $user)->orderBy($ordre)->get();
        foreach($objetsUser as $objet)
        {
            if($objet->article->categorie == 'livre'){
                $objets[] = [
                    'id' => $objet->article->id,
                    'categorie' => $objet->article->categorie,
                    'url' => $objet->article->url,
                    'nom' => $objet->article->nom,
                    'auteur'=> $objet->article->livre[0]->auteur, 
                    'description' => $objet->article->description,
                    'edition' => $objet->article->livre[0]->edition,
                    'ISBN' => $objet->article->livre[0]->ISBN,
                    'support' => $objet->article->livre[0]->type_de_support,
                    'date' => $objet->article->livre[0]->date_de_publication->format('d m Y')
                    
                ]; 
            }else if($objet->article->categorie == 'vetement'){
                $objets[] =  [
                    'id' => $objet->article->id,
                    'categorie' => $objet->article->categorie,
                    'url' => $objet->article->url,
                    'nom' => $objet->article->nom,
                    'description' => $objet->article->description,
                    'taille' => $objet->article->vetement[0]->taille
                ];
            }else{
                $objets[] =  [
                    'id' => $objet->article->id,
                    'categorie' => $objet->article->categorie,
                    'url' => $objet->article->url,
                    'nom' => $objet->article->nom,
                    'description' => $objet->article->description
                ];
            }
        } 
       
        return view('articles', [
            'articles' => $articles,
            'images' => $images,
            'objets' => $objets
        ]);
    }

    /** Formulaire : ajout nouvel article de utilisateur
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user()->id;
        $articles = Article::join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'livre')->where('user_id', '=', $user)->get();
        $livres = Livre::join("articles", "livres.article_id", "=", "articles.id" )->join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'livre')->where('user_id', '=', $user)->get();
        $request = request();
        return view('newarticle', [
            'articles' => $articles,
            'livres' => $livres
        ]);
    }

    /** Enregistrement nouvel article de utilisateur
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user()->id;

        $image = new Image();
        $request->validate([
            'url' => 'required|image|max:1024'                                       
        ]);
        
        // Stockage de image dans fichier storage
        $path = $request->url->store('public/uploads');
        $url = Storage::url($path);
        Image::create(['url' => $url]);

        // Ajout nouvel article
        $article = new Article();
        $article->categorie = $request->input('categorie');
        $article->nom = $request->input('nom');
        $article->description = $request->input('description');
        $article->url = $url;
        $article->save();

        $newid = $article->id;

        // Mise en relation entre article et utilisateur
        $possede = new Possede();
        $possede->user_id = $user;
        $possede->article_id = $newid;
        $possede->save();

        return redirect('/articles')->with('success', $article['nom'] . ' est ajouté');
    }

    /** Affichage de article a supprimer
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article, $id)
    {
        $user = Auth::user()->id;
        $articles = Article::join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'livre')->where('user_id', '=', $user)->get();
        $livres = Livre::join("articles", "livres.article_id", "=", "articles.id" )->join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'livre')->where('user_id', '=', $user)->get();
        $article = Article::find($id);
        return view('deletearticle', [
                'articles' => $articles,
                'livres' => $livres,
                'article' => $article
        ]);
    }

    /** Affichage de article a modifier
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article, $id)
    {
        $user = Auth::user()->id;
        $articles = Article::join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'livre')->where('user_id', '=', $user)->get();
        $livres = Livre::join("articles", "livres.article_id", "=", "articles.id" )->join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'livre')->where('user_id', '=', $user)->get();
        $article = Article::find($id);
        return view('updatearticle', [
                'articles' => $articles,
                'livres' => $livres,
                'article' => $article
        ]);
    }

    /** Enregistrement des modifications de article
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article, $id)
    {
        $article = Article::find($id);

        $data = $request->validate([
            'categorie' => 'required',
            'nom' => 'required',
            'description' => 'nullable'  
        ]);

        // Stockage de image dans fichier storage
        if($request->url != null){
            $image = new Image();
            $path = $request->url->store('public/uploads');
            $url = Storage::url($path);
            
            Image::create(['url' => $url]);
            $article->url = $url;
        }
          
        $article->update($data);
        

        return redirect('/articles')->with('success', $data['nom'] . " a bien été modifié !");
    }

    /** Suppression de article
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Article $article, $id)
    {
        $user = Auth::user()->id;

        // Suppression du lien entre article et utilisateur
        $possede = Possede::where('user_id', '=', $user)->where('article_id', '=', $id)->first();
        $possede->delete();
        
        $article = Article::find($id);
        $data = $request->validate([ 
            'id' => 'exists:id'
        ]);
        
        $article->delete($data);
        return redirect('/articles')->with('success', " L'article a bien été supprimé !");
    }
}
