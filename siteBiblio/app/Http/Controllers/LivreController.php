<?php

namespace App\Http\Controllers;

use App\Models\Livre;
use App\Models\Possede;
use App\Models\Article;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class LivreController extends Controller
{
    /** Affichege des livres de utilisateur
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::all();
        $user = Auth::user()->id;
        $request = request();
        $ordre = request()->input('ordre', 'nom');
        $articles = Article::join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'livre')->where('user_id', '=', $user)->get();  
        $livres = Livre::join("articles", "livres.article_id", "=", "articles.id" )->join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'livre')->where('user_id', '=', $user)->get();
           
        $objets = array();
        $objetsUser = Possede::join("articles", "possedes.article_id", "articles.id")->join("livres", "livres.article_id", "=", "articles.id")->where('user_id', '=', $user)->orderBy($ordre)->get();
        foreach($objetsUser as $objet)
        {
            if($objet->article->categorie == 'livre'){
                $objets[] = [
                    'id' => $objet->article->id,
                    'categorie' => $objet->article->categorie,
                    'url' => $objet->article->url,
                    'nom' => $objet->article->nom,
                    'auteur'=> $objet->article->livre[0]->auteur, 
                    'description' => $objet->article->description,
                    'edition' => $objet->article->livre[0]->edition,
                    'ISBN' => $objet->article->livre[0]->ISBN,
                    'support' => $objet->article->livre[0]->type_de_support,
                    'date' => $objet->article->livre[0]->date_de_publication->format('d m Y')
                    
                ]; 
            }else if($objet->article->categorie == 'vetement'){
                $objets[] =  [
                    'id' => $objet->article->id,
                    'categorie' => $objet->article->categorie,
                    'url' => $objet->article->url,
                    'nom' => $objet->article->nom,
                    'description' => $objet->article->description,
                    'taille' => $objet->article->vetement[0]->taille
                ];
            }else{
                $objets[] =  [
                    'categorie' => $objet->article->categorie,
                    'url' => $objet->article->url,
                    'nom' => $objet->article->nom,
                    'description' => $objet->article->description
                ];
            }

        }
        
        return view('biblio', [
                'articles' => $articles,
                'livres' => $livres,
                'images' => $images,
                'objets' => $objets
            ]);
    }

    /** Formulaire : ajout nouveau livre de utilisateur
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user()->id;
        $articles = Article::join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'livre')->where('user_id', '=', $user)->get();
        $livres = Livre::join("articles", "livres.article_id", "=", "articles.id" )->join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'livre')->where('user_id', '=', $user)->get();
        $request = request();
        return view('newlivre', [
            'articles' => $articles,
            'livres' => $livres
        ]);
    }

    /** Enregistrement nouveau livre de utilisateur
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user()->id;

        $image = new Image();
        $request->validate([
            'url' => 'required|image|max:1024'                                       
        ]);

        // Stockage de image dans fichier storage
        $path = $request->url->store('public/uploads');
        $url = Storage::url($path);
        Image::create(['url' => $url]);

        // Ajout nouveau livre
        $article = new Article();
        $article->categorie = 'livre';
        $article->nom = $request->input('titre');
        $article->description = $request->input('description');
        $article->url = $url;
        $article->save();

        $newid = $article->id;

        // Mise en relation entre article et utilisateur
        $possede = new Possede();
        $possede->user_id = $user;
        $possede->article_id = $newid;
        $possede->save();

        // Ajout des infos supplementaires du livre
        $livre = new Livre();
        $livre->article_id = $newid;
        $livre->auteur =  $request->input('auteur');
        $livre->edition =  $request->input('edition');
        $livre->type_de_support = $request->input('support');
        $livre->isbn =  $request->input('isbn');
        $livre->date_de_publication =  $request->input('publication');
        $livre->save();

        return redirect('/bibliotheque')->with('success', $article['nom'] . ' est ajouté');
    }

    /** Affichage du livre a supprimer
     * Display the specified resource.
     *
     * @param  \App\Models\Livre  $livre
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article, Livre $livre, $id)
    {
        $user = Auth::user()->id;
        $articles = Article::join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'livre')->where('user_id', '=', $user)->get();
        $livres = Livre::join("articles", "livres.article_id", "=", "articles.id" )->join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'livre')->where('user_id', '=', $user)->get();
        $article = Article::find($id);
        $livre = Livre::where('article_id', '=', $id)->first();
        return view('deletelivre', [
                'articles' => $articles,
                'livres' => $livres,
                'article' => $article,
                'livre' => $livre
        ]);
    }

    /** Affichage du livre a modifier
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Livre  $livre
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article, Livre $livre, $id)
    {
        $user = Auth::user()->id;
        $articles = Article::join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'livre')->where('user_id', '=', $user)->get();
        $livres = Livre::join("articles", "livres.article_id", "=", "articles.id" )->join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'livre')->where('user_id', '=', $user)->get();
        $article = Article::find($id);
        $livre = Livre::where('article_id', '=', $id)->first();
        return view('updatelivre', [
                'articles' => $articles,
                'livres' => $livres,
                'article' => $article,
                'livre' => $livre
        ]);
    }

    /** Enregistrement des modifications du livre
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Livre  $livre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Livre $livre, Article $article, $id)
    {
        $article = Article::find($id);
        $livre = Livre::where('article_id', '=', $id)->first();

        $data = $request->validate([
            'nom' => 'required',
            'description' => 'nullable',
            'auteur' => 'required',
            'edition' => 'nullable',
            'ISBN' => 'nullable',
            'date_de_publication' => 'nullable'
        ]);

         // Stockage de image dans fichier storage
         if($request->url != null){
            $image = new Image();
            $path = $request->url->store('public/uploads');
            $url = Storage::url($path);
            
            Image::create(['url' => $url]);
            $article->url = $url;
        }
        
        $article->update($data);
        $livre->update($data);
        return redirect('/bibliotheque')->with('success', $data['nom'] . " a bien été modifié !");
    }

    /** Suppression du livre
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Livre  $livre
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Article $article, Livre $livre, $id)
    {
        $user = Auth::user()->id;

        // Suppression du lien entre livre et utilisateur
        $possede = Possede::where('user_id', '=', $user)->where('article_id', '=', $id)->first();
        $possede->delete();
        
        $article = Article::find($id);
        $livre = Livre::where('article_id', '=', $id)->first();

        $data = $request->validate([ 
            'id' => 'exists:id'
        ]);
        
        $livre->delete($data);
        $article->delete($data);
        return redirect('/bibliotheque')->with('success', " Le livre a bien été supprimé !");
    }
}
