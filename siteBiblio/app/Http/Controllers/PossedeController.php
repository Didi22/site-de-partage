<?php

namespace App\Http\Controllers;

use App\Models\Possede;
use Illuminate\Http\Request;

class PossedeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Possede  $possede
     * @return \Illuminate\Http\Response
     */
    public function show(Possede $possede)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Possede  $possede
     * @return \Illuminate\Http\Response
     */
    public function edit(Possede $possede)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Possede  $possede
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Possede $possede)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Possede  $possede
     * @return \Illuminate\Http\Response
     */
    public function destroy(Possede $possede)
    {
        //
    }
}
