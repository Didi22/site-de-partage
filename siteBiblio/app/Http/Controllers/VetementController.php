<?php

namespace App\Http\Controllers;

use App\Models\Vetement;
use App\Models\Possede;
use App\Models\Article;
use App\Models\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VetementController extends Controller
{
    /** Affichage des vetements de utilisateur
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user()->id;
        $images = Image::all();
        $articles = Article::join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'vetement')->where('user_id', '=', $user)->get();
        $vetements = Vetement::join("articles", "vetements.article_id", "=", "articles.id" )->join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'vetement')->where('user_id', '=', $user)->get();
        $request = request();

        $ordre = request()->input('ordre', 'nom');
        $objets = array();
        $objetsUser = Possede::join("articles", "possedes.article_id", "articles.id")->join("vetements", "vetements.article_id", "=", "articles.id")->where('user_id', '=', $user)->orderBy($ordre)->get();
        foreach($objetsUser as $objet)
        {
            if($objet->article->categorie == 'livre'){
                $objets[] = [
                    'id' => $objet->article->id,
                    'categorie' => $objet->article->categorie,
                    'url' => $objet->article->url,
                    'nom' => $objet->article->nom,
                    'auteur'=> $objet->article->livre[0]->auteur, 
                    'description' => $objet->article->description,
                    'edition' => $objet->article->livre[0]->edition,
                    'ISBN' => $objet->article->livre[0]->ISBN,
                    'support' => $objet->article->livre[0]->type_de_support,
                    'date' => $objet->article->livre[0]->date_de_publication->format('d m Y')
                    
                ]; 
            }else if($objet->article->categorie == 'vetement'){
                $objets[] =  [
                    'id' => $objet->article->id,
                    'categorie' => $objet->article->categorie,
                    'url' => $objet->article->url,
                    'nom' => $objet->article->nom,
                    'description' => $objet->article->description,
                    'taille' => $objet->article->vetement[0]->taille
                ];
            }else{
                $objets[] =  [
                    'id' => $objet->article->id,
                    'categorie' => $objet->article->categorie,
                    'url' => $objet->article->url,
                    'nom' => $objet->article->nom,
                    'description' => $objet->article->description
                ];
            }
        } 
    
        return view('vetements', [
                'articles' => $articles,
                'vetements' => $vetements,
                'images' => $images,
                'objets' => $objets
            ]);
    }

    /** Formulaire : ajout nouveau vetement de utilisateur
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $user = Auth::user()->id;
        $articles = Article::join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'vetement')->where('user_id', '=', $user)->get();
        $vetements = Vetement::join("articles", "vetements.article_id", "=", "articles.id" )->join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'vetement')->where('user_id', '=', $user)->get();
        $request = request();
        return view('newvetement', [
            'articles' => $articles,
            'vetements' => $vetements
        ]);
    }

    /** Enregistrement nouveau vetement de utilisateur
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user()->id;

        $image = new Image();
        $request->validate([
            'url' => 'required|image|max:1024'                                       
        ]);

         // Stockage de image dans fichier storage
        $path = $request->url->store('public/uploads');
        $url = Storage::url($path);
        Image::create(['url' => $url]);

        // Ajout du nouveau vetement
        $article = new Article();
        $article->categorie = 'vetement';
        $article->nom = $request->input('nom');
        $article->description = $request->input('description');
        $article->url = $url;
        $article->save();

        $newid = $article->id;

        // Mise en relation entre vetement et utilisateur
        $possede = new Possede();
        $possede->user_id = $user;
        $possede->article_id = $newid;
        $possede->save();

        // Ajout des infos supplementaires du vetement
        $vetement = new Vetement();
        $vetement->article_id = $newid;
        $vetement->taille =  $request->input('taille');
        $vetement->save();

        return redirect('/dressing')->with('success', $article['nom'] . ' est ajouté');

    }

    /** Affichage du vetement a supprimer
     * Display the specified resource.
     *
     * @param  \App\Models\Vetement  $vetement
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article, Vetement $vetement, $id)
    {
        $user = Auth::user()->id;
        $articles = Article::join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'vetement')->where('user_id', '=', $user)->get();
        $vetements = Vetement::join("articles", "vetements.article_id", "=", "articles.id" )->join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'vetement')->where('user_id', '=', $user)->get();
        $article = Article::find($id);
        $vetement = Vetement::where('article_id', '=', $id)->first();
        return view('deletevetement', [
                'articles' => $articles,
                'vetements' => $vetements,
                'article' => $article,
                'vetement' => $vetement
        ]);
    }

    /** Affichage du vetement a modifier
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vetement  $vetement
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article, Vetement $vetement, $id)
    {
        $user = Auth::user()->id;
        $articles = Article::join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'vetement')->where('user_id', '=', $user)->get();
        $vetements = Vetement::join("articles", "vetements.article_id", "=", "articles.id" )->join("possedes", "articles.id", "=", "possedes.article_id")->where('categorie', 'vetement')->where('user_id', '=', $user)->get();
        $article = Article::find($id);
        $vetement = Vetement::where('article_id', '=', $id)->first();
        return view('updatevetement', [
                'articles' => $articles,
                'vetements' => $vetements,
                'article' => $article,
                'vetement' => $vetement
        ]);
    }

    /** Enregistrement des modifications du vetement
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vetement  $vetement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article, Vetement $vetement, $id)
    {
        $article = Article::find($id);
        $vetement = Vetement::where('article_id', '=', $id)->first();

        $data = $request->validate([
            'nom' => 'required',
            'description' => 'nullable',
            'taille' => 'required'
        ]);

         // Stockage de image dans fichier storage
         if($request->url != null){
            $image = new Image();
            $path = $request->url->store('public/uploads');
            $url = Storage::url($path);
            
            Image::create(['url' => $url]);
            $article->url = $url;
        }
                  
        $article->update($data);
        $vetement->update($data);
        return redirect('/dressing')->with('success', $data['nom'] . " a bien été modifié !");
    }

    /** Suppression du vetement
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vetement  $vetement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Article $article, Vetement $vetement, $id)
    {
        $user = Auth::user()->id;

        // Suppression du lien entre vetement et utilisateur
        $possede = Possede::where('user_id', '=', $user)->where('article_id', '=', $id)->first();
        $possede->delete();
        
        $article = Article::find($id);
        $vetement = Vetement::where('article_id', '=', $id)->first();

        $data = $request->validate([ 
            'id' => 'exists:id'
        ]);
        
        $vetement->delete($data);
        $article->delete($data);
        return redirect('/dressing')->with('success', " Le vêtement a bien été supprimé !");
    }
}
