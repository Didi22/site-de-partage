<?php

namespace App\Http\Controllers;

use App\Models\Ami;
use App\Models\User;
use App\Models\Article;
use App\Models\Livre;
use App\Models\Vetement;
use App\Models\Image;
use App\Models\Possede;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use App\Notifications\NewDemand;
use App\Notifications\NewFriend;
use Illuminate\Notifications\Notifiable;

class AmiController extends Controller
{
    /** Affichage des articles des amis
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::all();
        $user = Auth::user()->id;
        $amis = Ami::join('users', "amis.ami_id", "=", "users.id")->where('statut', '=', 'ami')->where('user_id', '=', $user)->get();
        $objets = array();
       
        foreach($amis as $ami){
            $objetsRequete = Possede::where('user_id', '=', $ami->ami_id)->get();
            foreach ($objetsRequete as $objet){

                if($objet->article->categorie == 'livre'){
                    $objets[] = [
                        'ami_id' => $objet->user_id,
                        'categorie' => $objet->article->categorie,
                        'url' => $objet->article->url,
                        'nom' => $objet->article->nom,
                        'auteur'=> $objet->article->livre[0]->auteur, 
                        'description' => $objet->article->description,
                        'edition' => $objet->article->livre[0]->edition,
                        'ISBN' => $objet->article->livre[0]->ISBN,
                        'support' => $objet->article->livre[0]->type_de_support,
                        'date' => $objet->article->livre[0]->date_de_publication->format('d m Y')
                    ]; 
                }else if($objet->article->categorie == 'vetement'){
                    $objets[] =  [
                        'ami_id' => $objet->user_id,
                        'categorie' => $objet->article->categorie,
                        'url' => $objet->article->url,
                        'nom' => $objet->article->nom,
                        'description' => $objet->article->description,
                        'taille' => $objet->article->vetement[0]->taille
                    ];
                }else{
                    $objets[] =  [
                        'ami_id' => $objet->user_id,
                        'categorie' => $objet->article->categorie,
                        'url' => $objet->article->url,
                        'nom' => $objet->article->nom,
                        'description' => $objet->article->description
                    ];
                }
            }
        } 
    
        //si utilisateur sans ami : redirection sur une autre page
        $ami = Ami::where('statut', '=', 'ami')->where(function ($query) {
            $user = Auth::user()->id;
            $query->where('user_id', '=', $user)->orwhere('ami_id', '=', $user);
        })->first();

        if($ami == null){
            return view('newami');
        }
        
            return view('amis', [
            'amis' => $amis,
            'images' => $images, 
            'objets' => $objets
            ]);    
    }

    /** Demande amitie 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = Auth::user()->id;

        $pseudo = $request->input('pseudo');
        $mail = $request->input('email');
        $ami = User::where('name', '=', $pseudo)->orwhere('email', '=', $mail)->first();
        $liens1;
        $liens2;
       
        if($ami != null) {
            $lien1 = Ami::where('user_id', '=', $ami->id)->where('ami_id', '=', $user)->first();
            $lien2 = Ami::where('user_id', '=', $user)->where('ami_id', '=', $ami->id)->first();

            if($request->input('demande')){
                // Creation de relation entre deux utilisateurs
                $amitie = new Ami();
                $amitie->user_id = $user;
                $amitie->ami_id = $ami->id;
                $amitie->statut = 'demande_en_cours';
                $amitie->save();

                // Envoie de notifications des demandes amis
                $ami->notify(new NewDemand());
                $ami->notify(new \App\Notifications\NewFriend($user));
                return back()->with('success', " La demande d'ami est envoyée");
            }else if($request->input('suppression')){
                // Suppression amitie 
                if($lien1 != null){
                    $lien1->delete(); 
                }
                if($lien2 != null){
                    $lien2->delete(); 
                } 
            }
                return back()->with('success', "L'amitié est supprimée");
        }
        
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /** Affichage des demandes amicales
     * Display the specified resource.
     *
     * @param  \App\Models\Ami  $ami
     * @return \Illuminate\Http\Response
     */
    public function show(Ami $ami)
    {
        $user = Auth::user()->id;
        $amis = Ami::join('users', "amis.user_id", "=", "users.id")->where('statut', '=', 'demande_en_cours')->where('ami_id', '=', $user)->get(); 
    
        foreach($amis as $ami){
            $demandeurs = $ami->user_id; 
            if($demandeurs != null){
                return view('ajoutami', [
                    'amis' => $amis,
                    'demandeurs' => $demandeurs
                ]); 
            }
        }
        
        return view('newami');
    }

    /** Acceptation ou refus amitie
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ami  $ami
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Article $article, Ami $ami)
    {
        $name = Auth::user()->name;
        $user = Auth::user()->id;
        $users = User::join('amis', "users.id", "=", "amis.ami_id")->where('statut', '=', 'demande_en_cours')->get();
        $amis1 = Ami::join('users', "amis.ami_id", "=", "users.id")->where('statut', '=', 'demande_en_cours')->where('ami_id', '=', $user)->get();
        $amis2 = Ami::join('users', "amis.user_id", "=", "users.id")->where('statut', '=', 'demande_en_cours')->where('ami_id', '=', $user)->get();
    
        foreach($amis1 as $ami){
            if($ami->name != $name){
                $amis = $amis1;
            }
        }
        foreach($amis2 as $ami){
            if($ami->name != $name){
                $amis = $amis2;
            }
        }

        $ami = $request->input('demande');
        $lien = Ami::where('user_id', '=', $ami)->where('ami_id', '=', $user)->first();

        if($request->input('statut')){
            // Acceptation de amitie
            $data = $request->validate([
                'user_id' => 'nullable',
                'ami_id' => 'nullable',
                'statut' => 'required'
            ]);
            $lien->update($data);  

            $amitie = new Ami();
            $amitie->user_id = $user;
            $amitie->ami_id = $ami;
            $amitie->statut = 'ami';
            $amitie->save();

            //Suppression de la notification
            $notif = \DB::table('notifications')->where('notifiable_id', '=', $user)->where('data', '=', '{"user":'.$ami.'}' )->first();
            if($notif != null){
                \DB::table('notifications')->where('notifiable_id', '=', $user)->where('data', '=', '{"user":'.$ami.'}' )->delete();
            }

            return back()->with('success', " La demande d'ami est acceptée"); 
        }else if($request->input('non')){
            // Refus de amitie
            if($lien != null){
                $lien->delete();   

                //Suppression de la notification
                $notif = \DB::table('notifications')->where('notifiable_id', '=', $user)->where('data', '=', '{"user":'.$ami.'}' )->first();
                if($notif != null){
                   \DB::table('notifications')->where('notifiable_id', '=', $user)->where('data', '=', '{"user":'.$ami.'}' )->delete();
              }
            }
            return back()->with('success', " La demande d'ami est refusée"); 
        }else {
             return view('ajoutami', [
                    'users' => $users,
                    'user' => $user,
                    'amis' => $amis,
                    'ami' => $ami,
                    'lien' => $lien
                    ]);
        }
        return view('ajoutami', [
            'users' => $users,
            'user' => $user,
            'amis' => $amis,
            'ami' => $ami,
            'lien' => $lien
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ami  $ami
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ami $ami)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ami  $ami
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Ami $ami)
    {
        //
    }
}
