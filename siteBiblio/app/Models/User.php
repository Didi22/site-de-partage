<?php

namespace App\Models;

use App\Models\Ami;
use App\Models\User;
use App\Models\Article;
use App\Models\Livre;
use App\Models\Vetement;
use App\Models\Image;
use App\Models\Possede;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    public function article(){
        return $this->hasMany(Article::class);
    }

    public function amis(){
        return $this->hasMany(User::class);
    }

    public function possede(){
        return $this->hasMany(Possede::class);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
