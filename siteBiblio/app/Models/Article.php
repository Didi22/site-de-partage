<?php

namespace App\Models;

use App\Models\Ami;
use App\Models\User;
use App\Models\Article;
use App\Models\Livre;
use App\Models\Vetement;
use App\Models\Image;
use App\Models\Possede;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['categorie', 'nom', 'description', 'url'];

    public $timestamps = false;
    
    use HasFactory;

    public function livre(){
        return $this->hasMany(Livre::class);
    }

    public function vetement(){
        return $this->hasMany(Vetement::class);
    }

    public function possede(){
        return $this->hasMany(Possede::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
