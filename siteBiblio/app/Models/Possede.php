<?php

namespace App\Models;

use App\Models\Ami;
use App\Models\User;
use App\Models\Article;
use App\Models\Livre;
use App\Models\Vetement;
use App\Models\Image;
use App\Models\Possede;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Possede extends Model
{
    public $timestamps = false;

    public function article(){
        return $this->belongsTo(Article::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
    
    use HasFactory;
}
