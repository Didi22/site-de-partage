<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vetement extends Model
{
    protected $fillable = ['taille', 'id'];

    public $timestamps = false;
    
    use HasFactory;

    public function article(){
        return $this->belongsTo(Article::class);
    }
}
