<?php

namespace App\Models;

use App\Models\Ami;
use App\Models\User;
use App\Models\Article;
use App\Models\Livre;
use App\Models\Vetement;
use App\Models\Image;
use App\Models\Possede;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Livre extends Model
{
    protected $fillable = ['auteur', 'date_de_publication', 'edition', 'id', 'ISBN', 'titre', 'type_de_support'];

    public $timestamps = false;

    protected $casts = ['date_de_publication' => 'datetime:d m Y'];

    use HasFactory;

    public function article(){
        return $this->belongsTo(Article::class);
    }
}
