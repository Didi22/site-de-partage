# **Projet personnel Web et mobile : Site de partage**

Site web permettant l'organisation de sa propre bibliothèque, d'en faire un inventaire et avoir la possibilité de réaliser des emprunts entre connaissances. Le site élargit ce concept à d'autres biens qui restent dans les placards (bricolage, vêtements ...).

Le développement est effectué en PHP, en Javascript et à l'aide du framework Laravel. 

Les données sont gérées depuis une base de données MySQL. 

Ce dépôt contient :

- un cahier des charges
- des mockups version desktop et version mobile 
- un schéma de la base de données : MCD
- un jeu de données avec les scripts de création des schémas de la base de données
- le code source du bachend et du frontend

## **Documentation utilisateur**

Une inscription est nécessaire pour accéder aux fonctionnalités de partage. L'utilisateur effectue un inventaire de sa bibliothèque, de ses possessions littéraires et matériels à l'aide des formulaires. Puis il rajoute ses proches afin de rendre accessible ses livres et ses articles à ces derniers.
Dans le menu, on trouvera :
- une page répertoriant les livres avec la gestion de la bibliothèque (ajout, suppression de livres)
- deux autres sur le même principe avec les vêtements et les articles en tous genres (bricolage, jeux, électroménager ...)
- une page listant les proches de l'utilisateur avec leurs possessions
- une page pour réaliser une demande d'ami
- une page pour accepter une amitié
- Une page descriptive hors connexion

## **Recherches : envoi d'une notification**


1. Création d'une classe (NewFriend) avec le mot-clé extends et la classe Notification
```php
    <?php

    namespace App\Notifications;

    class NewFriend extends Notification
    {
        use Queueable;
    }
```

2. Cette classe contient :
- un constructeur où on transmet une instance de l'utilisateur concerné, 
```php
    protected $user;
    public function __construct($user)
    {
        $this->user = $user;
    }
```
- une méthode définissant le canal de notification (via) 
```php
    public function via($notifiable)
    {
        return ['database'];
    }
```
- et une méthode toArray pour transmettre cet utilisateur.
```php
    public function toArray($notifiable)
    {
       
        return [
            'user' => $this->user
        ];
    }
```

3. Création de la notification :
```php
    $ami->notify(new App\Notifications\NewFriend($user));
```
