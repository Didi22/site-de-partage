 <x-app-layout>  <!-- syntaxe qui renvoie au composant/vue de base layout.app (structure de base de la page) -->
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Go Green') }}
        </h2>
    </x-slot>
    
   
    @if(session()->has('success'))	
		<div class="alert alert-success" role="alert">
			<h4 class="alert-heading">{{ session()->get('success') }}</h4>
		</div>
	@endif
    <a href="javascript: switchForm();" id="lienachanger">+</a> 
    <div class="ml-9 tabarticles py-12 flex flex-row">
        <div id="formcacher"  style="display: none;">
            <div class="colorform p-6 border-b border-gray-200 ">
                <div class="article">
                    <h1>Ajouter un livre</h1>
                    <form method="POST" action="/livre/nouveau" enctype="multipart/form-data" class="mt-5">
                    @csrf
                        <label>Titre</label>
                        <textarea rows="1" name="titre" placeholder="titre" class="form-control"></textarea>
                        <label>Auteur</label>
                        <textarea rows="1" name="auteur" placeholder="auteur" class="form-control"></textarea>
                        <label>Description</label>
                        <textarea rows="1" name="description" placeholder="description" class="form-control"></textarea>
                        <label>Support</label>
                        <select name="support" size="1" id="supportChoix" class="form-control">
                            <option value="livre">livre</option>
                            <option value="BD">BD</option>
                            <option value="manga">manga</option>
                            <option value="revue">revue</option>
                            <option value="autre">autre</option>
                        </select>
                        <label>Edition</label>
                        <textarea rows="1" name="edition" placeholder="edition" class="form-control"></textarea>
                        <label>ISBN</label>
                        <textarea rows="1" name="isbn" placeholder="ISBN" class="form-control"></textarea>
                        <label>Date de publication</label>
                        <input type="date" name="publication" class="form-control">
                        <div class="pt-4">
                            <label>Image</label>
                            <input type="file" name="url">
                        </div>
                        <div class="bouton">
                            <input type="submit" value="Ajouter" class="btn btn-primary ml-9 mt-5"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="flex flex-column">
            <p> 
                <span class="underline">Trier par </span> : <a href="/bibliotheque/?ordre=nom"> Titre </a> | <a href="/bibliotheque/?ordre=auteur"> Auteur </a> | <a href="/bibliotheque/?ordre=type_de_support"> Support </a>
            </p>
            
            <div class="grid lg:grid-cols-4 sm:grid-cols-1 md:grid-cols-2">
                @foreach ($objets as $objet)
                    @if($objet['categorie'] == 'livre')
                    <div class="m-8 flex flex-column">
                            <p class="text-left"><a href="/livre/delete/{{ $objet['id'] }}">x</a></p>
                            <p><img src="{{ asset($objet['url']) }}" class="transform hover:scale-110 transition duration-200 mt-8"></p>
                            <p><a href="/livre/update/{{ $objet['id'] }}"> {{ $objet['nom'] }} </a></p> 
                            <p>{{ $objet['auteur'] }} </p>
                            <p>{{ $objet['description'] }} </p>
                            <p>{{ $objet['edition'] }} </p>
                            <p>{{ $objet['ISBN'] }} </p>
                            <p>{{ $objet['support'] }} </p>
                            <p>{{ $objet['date'] }} </p> 
                    </div> 
                    @endif
                @endforeach                  
            </div>  
        </div> 
    </div>
</x-app-layout>

<script type="text/javascript">

    function switchForm()
    {
        // Montre ou cache le formulaire 
        let div = document.getElementById('formcacher');
        if(div.style.display == 'none'){
            div.style.display = 'block';
        }else{
            div.style.display = 'none';
        }

        let lien = document.getElementById('lienachanger');
        let contenu = "<-";
        let contenu2 = "+";
        if(lien.innerHTML == '+'){
            lien.innerHTML = contenu;
        }else{
            lien.innerHTML = contenu2;
        }
    }
</script>