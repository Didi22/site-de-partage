<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Go Green') }}
        </h2>
    </x-slot>

    <div class="py-12"> 
        <div class="lg:flex lg:flex-row">
            <div class="menuImages hidden md:block">
                <figure><a href="/bibliotheque">
                    <img src="images/livre.png" alt="livre"/>
                    <figcaption>Vos livres</figcaption> </a>
                </figure>
                <figure><a href="/articles">
                    <img src="images/machine.png" alt="article"/>
                    <figcaption>Vos articles</figcaption> </a>
                </figure>
                <figure><a href="/dressing">
                    <img src="images/vetement.png" alt="vetement"/>
                    <figcaption>Vos vêtements</figcaption> </a>
                </figure>
                <figure><a href="/livre/ajout">
                    <img src="images/livreplus.png" alt="livre+"/>
                    <figcaption>Ajouter un livre</figcaption> </a>
                </figure>
                <figure><a href="/article/ajout">
                    <img src="images/machineplus.png" alt="article+"/>
                    <figcaption>Ajouter un article</figcaption> </a>
                </figure>
                <figure><a href="/vetement/ajout">
                    <img src="images/vetementplus.png" alt="vetement+"/>
                    <figcaption>Ajouter un vêtement</figcaption> </a>
                </figure>
                <figure><a href="/amis">
                    <img src="images/amis.png" alt="amis"/>
                    <figcaption>Vos amis</figcaption> </a>
                </figure>
                <figure><a href="/ami/ajout">
                    <img src="images/ami.png" alt="ami"/>
                    <figcaption>Ajouter un ami</figcaption> </a>
                </figure>
            </div>
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="lg:flex lg:flex-col">
                        <div class="presentation p-6 bg-white border-b border-gray-200">
                            Bonjour {{ auth()->user()->name }} ! <br> </br>

                        @unless(auth()->user()->unReadNotifications->isEmpty())
                            <span class="animate-pulse">
                                <p> <a href="/ami/ajout">  Vous avez {{ auth()->user()->unReadNotifications->count() }} nouvelle(s) demande(s) d'ami(s) </a></p>
                            </span>
                        @endunless
                        </div>
                        <div class="ml-9 tabarticles py-12">
                            <div class="lg:flex lg:flex-row">
                                <form method="POST" action="/dashboard" class="mt-5 flex flex-col">
                                        @csrf
                                        <label class="pt-4">Recherche d'un article</label>
                                        <input type="search" name="rechercheArticle" class="shadow border-1" placeholder="Recherche">

                                        <label class="pt-4">Recherche d'un livre</label>
                                        <input type="search" name="rechercheLivre" class="shadow border-1" placeholder="Recherche">

                                        <label class="pt-4">Recherche d'un vêtement</label>
                                        <input type="search" name="rechercheVetement" class="shadow border-1" placeholder="Recherche">

                                        <input type="submit" value="Recherche" class="btn btn-primary ml-9 mt-4 mx-auto"/>
                                </form>

                                @if(isset($recherche))
                                <div class="grid lg:grid-cols-3 sm:grid-cols-1 md:grid-cols-2">
                                    @foreach ($recherche as $donnee)
                                        <div class="m-8 flex flex-column">
                                            <p><img src="{{ asset($donnee['url']) }}" class="transform hover:scale-110 transition duration-200 mt-8"></p>
                                            <p>{{ $donnee->nom }}</p>
                                            <p>{{ $donnee->description }}</p>
                                            <p>{{  $donnee->categorie }}</p>
                                            @if($donnee->categorie == 'livre')
                                                <p>{{ $donnee->auteur }}</p>
                                                <p>{{ $donnee->edition }}</p>
                                                <p>{{ $donnee->ISBN }}</p>
                                                <p>{{ $donnee->type_de_support }}</p>
                                                <p>{{ $donnee->date_de_publication }}</p>
                                            @endif
                                            @if($donnee->categorie == 'vetement')
                                            <p>{{ $donnee->taille }}</p>
                                            @endif
                                            <p>Cet article appartient à : {{ $donnee->user->name }}</p>
                                        </div>
                                    @endforeach                            
                                </div>
                                @else
                                <div class="presentation p-16 m-16 flex flex-col">
                                    <p> Rechercher un article, un livre ou un vêtement parmi les biens des autres utilisateurs de ce site.</p>
                                    <p>Si un bien d'un autre utilisateur vous intéresse, vous pouvez lui envoyer une <a href="/ami/recherche">demande d'amitié</a>.</p>
                                    <p>Consulter vos <a href="/bibliotheque">livres</a>, vos <a href="/articles">articles</a> ou bien encore vos <a href="/dressing">vêtements</a>.</p>
                                    <p>Vous souhaitez rajouter des biens : <a href="/livre/ajout">livres</a>,  <a href="/article/ajout">articles</a> ou  <a href="vetement/ajout">vêtements</a>.</p>
                                </div>
                                @endif
                            </div>    
                        </div> 
                    </div>
                    <div class="presentation">
                        <p class="pt-4">Vos amis :</p> <p class="text-xs">(cliquez pour voir leurs biens)</p>
                        <a href="/amis">
                        @foreach($amis as $ami)
                            <p>  {{ $ami->name }}  </p>
                        @endforeach</a>
                    </div>
                </div>
            </div> 
        </div>
    <div>
    <div>
        <p class="presentation pt-4">Les derniers articles ajoutés sont : </p> 
        <div class="grid lg:grid-cols-4 sm:grid-cols-1 md:grid-cols-2"> 
            @foreach($objets as $objet)
            <div class="presentation tabarticles m-8 flex flex-column"> 
                <p>{{ $objet->user->name }} a ajouté dans sa liste: </p>
                <p><img src="{{ asset($objet['url']) }}" class="transform hover:scale-110 transition duration-200 mt-8"></p>
                <p> {{ $objet->nom }} </p> 
                @if($objet->categorie == 'livre')
                    <p>{{ $objet->livre[0]->auteur }}</p>
                    <p>{{ $objet->description }}</p>
                    <p>{{ $objet->livre[0]->edition }}</p>
                    <p>{{ $objet->livre[0]->ISBN }}</p>
                    <p>{{ $objet->livre[0]->type_de_support }}</p>
                    <p>{{ $objet->livre[0]->date_de_publication->format('d m Y') }}</p> 
                @endif
                @if($objet->categorie == 'vetement')
                    <p>{{ $objet->vetement[0]->taille }}</p>
                @endif
            </div>
            @endforeach    
        </div>
    </div>
</x-app-layout>
