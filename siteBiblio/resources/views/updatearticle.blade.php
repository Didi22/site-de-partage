<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Go Green') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <h1>Modifier l'article {{ $article->nom }}</h1>
                    <form method="POST" enctype="multipart/form-data" class="mt-5">
                        @csrf 
                        @method('patch')
                        @if(session('success'))
                        <p>{{ session('success') }}</p>
                        @endif 
                            <label>Nom</label>
                            <input type="text" name="nom" value="{{ $article->nom }}" placeholder="Nom" class="form-control" />
                            <label>Catégorie</label>
                            <select name="categorie" size="1" id="articleChoix">
                                    <option value="{{$article->categorie}}" @if($article->categorie == $article->id) selected="selected" @endif>{{ $article->categorie }}</option>
                                    <option value="bricolage">Bricolage</option>
                                    <option value="electromenager">Electromenager</option>
                                    <option value="aide">Aide</option>
                                    <option value="jeux">Jeux</option>
                                    <option value="sport">Sport</option>
                                    <option value="musique">Musique</option>
                                    <option value="autre">Autre</option>
                             </select>
                             <div>
                                <label>Description</label>
                                <input type="text" name="description" value="{{ $article->description }}" class="form-control" />
                                <label>Image</label>
                                <input type="file" name="url" value="{{ $article->url }}" class="form-control" />
                            </div>
                            <input type="submit" value="modifier" class="btn btn-primary mt-5"/>
                        </form>
                    </div>
            </div>
        </div>
    </div>
</x-app-layout>