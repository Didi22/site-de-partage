<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Go Green</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <style>
            body {
                font-family: 'MV Boli';
            }
            .presentation a{
                color: #045f48;
                text-decoration: underline;
            }
            .presentation a:hover{
                font-size: 1.6em;
                color: #045f48;
                font-weight: bold;
            }
            .presentation{
                margin-left: 2em;
                font-size: 1.2em;
            }
            .tabarticles img{
                height: 20em;
                width: auto; 
                margin: 2em;
            }
            .miniature img{
                height: 20em;
                width: auto; 
            }
            .menuImages img{
                height: auto;
                width: 5em; 
                margin-top: 2em;
                padding-right: 1.5em;
            }
            footer{
                background-color: #045f48;
            }
            .colorform{
                background-color: #045f48;
                color: white;
            }
            .bouton{
                text-align: right;
            }
            #lienachanger, #lienachanger2, #lienachanger3{
                font-size: 3em;
                color: #045f48;
            }
            @media (max-width: 768px){
                .tabarticles img{
                    height: 5em;
                    width: auto;
                }
                .presentation a{
                    font-size: 1.2em;
                    color: #045f48;
                    font-weight: bold;
                }
                .presentation{
                    font-size: 0.90em;
                    margin-left: 0;
                    padding-left: 0;
                    margin-right: 0;
                    padding-right: 0;
                    margin-bottom: 0;
                    padding-bottom: 0;
                }
            }
        </style>
    </head>
    <body class="antialiased">
       
        <div class="min-h-screen bg-white">
            @include('layouts.navigation')

            <!-- Page Heading -->
            <header class="bg-white">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header>

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div>
    </body>
    <footer>
            <p class="text-center text-white">Copyright@2021</p>
            <p class="sm:text-sm text-center text-white"><a href="/confidentialite">Politiques de confidentialité</a> | <a href="/cookies">Politiques des cookies</a><p>
    </footer>
</html>
