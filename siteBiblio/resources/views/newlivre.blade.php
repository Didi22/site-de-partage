<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Go Green') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="colorform p-6 border-b border-gray-200">
                    <div class="article">
                        <h1>Ajouter un livre</h1>
                        <form method="POST" action="/livre/nouveau" enctype="multipart/form-data" class="mt-5">
                        @csrf
                            <label>Titre</label>
                            <textarea rows="1" name="titre" placeholder="titre" class="form-control"></textarea>
                            <label>Auteur</label>
                            <textarea rows="1" name="auteur" placeholder="auteur" class="form-control"></textarea>
                            <label>Description</label>
                            <textarea rows="1" name="description" placeholder="description" class="form-control"></textarea>
                            <label>Support</label>
                            <select name="support" size="1" id="supportChoix" class="form-control">
                                <option value="livre">livre</option>
                                <option value="BD">BD</option>
                                <option value="manga">manga</option>
                                <option value="revue">revue</option>
                                <option value="autre">autre</option>
                            </select>
                            <label>Edition</label>
                            <textarea rows="1" name="edition" placeholder="edition" class="form-control"></textarea>
                            <label>ISBN</label>
                            <textarea rows="1" name="isbn" placeholder="ISBN" class="form-control"></textarea>
                            <label>Date de publication</label>
                            <input type="date" name="publication" class="form-control">
                            <div class="pt-4">
                                <label>Image</label>
                                <input type="file" name="url">
                            </div>
                            <div class="bouton">
                                <input type="submit" value="Ajouter" class="btn btn-primary ml-9 mt-5"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>