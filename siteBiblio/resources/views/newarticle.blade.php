<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Go Green') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="colorform p-6 border-b border-gray-200">
                    <div class="article">
                        <h1>Ajouter un article</h1>
                        <form method="POST" action="/article/nouvel" enctype="multipart/form-data" class="mt-5">
                        @csrf
                            <label>Catégorie</label>
                            <select name="categorie" size="1" id="categorieChoix" class="form-control">
                                <option value="bricolage">Bricolage</option>
                                <option value="electromenager">Electromenager</option>
                                <option value="aide">Aide</option>
                                <option value="jeux">Jeux</option>
                                <option value="sport">Sport</option>
                                <option value="musique">Musique</option>
                                <option value="autre">Autre</option>
                            </select>
                            <label>Nom</label>
                            <textarea rows="1" name="nom" placeholder="nom" class="form-control"></textarea>
                            <label>Description</label>
                            <textarea rows="1" name="description" placeholder="description" class="form-control"></textarea>
                            <div class="pt-4">
                                <label>Image</label>
                                <input type="file" name="url">
                            </div>
                            <div class="bouton">
                                <input type="submit" value="Ajouter" class="btn btn-primary mt-5"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>