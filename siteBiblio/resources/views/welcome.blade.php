<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Go Green</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

         <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
      
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>


        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
        </style>

        <style>
            body {
                font-family: 'MV Boli';
            }
            .titre{
                font-family: 'Brush Script MT', cursive;
                color: white;
                text-shadow: 5px 5px #045f48;
            }
            .ombre{
                text-shadow: 2px 2px white;
            }
            .ombre2{
                text-shadow: 2px 2px black;
            }
            .custom-img{
                background-image: url("images/ecolo2.jpg");
                background-repeat: no-repeat;
            }
            .custom-img2{
                background-image: url("images/planche.jpg");
                background-repeat: no-repeat;  
            } 
            .custom-img3{
                background-image: url("images/forest.jpg");
                background-repeat: no-repeat;  
            } 
            .custom-img4{
                background-image: url("images/vase.jpg");
                background-repeat: no-repeat;  
            } 
            .custom-img5{
                background-image: url("images/gift.jpg");
                background-repeat: no-repeat;  
            } 
            footer{
                background-color: #045f48;
            }
            /* .images img {
                box-shadow: 20px 20px 5px black;
            } */
            @media (max-width: 640px) {
                .custom-img{
                    background-image: url("images/planche.jpg");
                    background-repeat: no-repeat;  
                } 
                .custom-img4{
                    background-image: url("images/forest.jpg");
                    background-repeat: no-repeat;  
                }
                .custom-img5{
                    background-image: url("images/forest.jpg");
                    background-repeat: no-repeat;  
                }
                .divhand{
                    display: flex;
                    flex-direction: column-reverse;
                    color: white;
                } 
                .connexion a{
                    color: white; 
                }
                .ombre{
                    text-shadow: 1px 1px black;
                }
            }
        </style>
    </head>
    <header  class="flex items-center justify-center h-screen bg-fixed bg-center bg-cover custom-img">
        <div class="p-5 text-2xl text-black bg-opacity-50 rounded-xl">
            <div class="titre pt-32 text-7xl font-bold"> Bienvenue sur Go Green ! </div>
            <div class="ombre flex items-center justify-center">Site de partage entre amis</div>
            <div class="bg-gray-400 rounded-full bg-opacity-50 m-8 flex flex-column items-center justify-center">
                <div class="m-6"><a href="{{ route('register') }}" class="focus:outline-none text-white text-sm py-2.5 px-5 rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg">S'inscrire</a></div>
                <div class="m-6"><a href="{{ route('login') }}" class="underline">Déjà inscrit ? Se connecter</a></div>
            </div>
            <div class="pt-2 text-center">Plus d'infos</div>
            <div class="pt-2 text-center animate-bounce text-7xl font-bold"> ⇩ </div>
        </div>

    </header>
    <body class="antialiased">
        <div  class="flex items-center justify-center h-screen bg-fixed bg-center bg-cover custom-img2">
            <div class="lg:flex justify-center pt-8 sm:justify-start sm:pt-0">
                <p  class="ombre2 p-5 sm:text-2xl lg:text-4xl bg-opacity-50 rounded-xl text-white font-bold">Le poids moyen de l'ensemble des équipements, meubles et électroménagers dans une maison est de <span class="underline">2,5 tonnes</span>.</p>
                <p  class="ombre2 p-5 sm:text-2xl lg:text-4xl bg-opacity-50 rounded-xl text-white font-bold"><span class="underline">45 tonnes</span> de matières premières sont mobilisées à leur fabrication</p>
                <p  class="ombre2 p-5 sm:text-2xl lg:text-4xl bg-opacity-50 rounded-xl text-white font-bold"><span class="underline">6 tonnes</span> de CO2 est émis pendant leur cycle de fabrication.</p>
            </div>
        </div>
        <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0">
            @if (Route::has('login'))
                <div class="connexion hidden fixed top-0 right-0 px-6 py-4 d-block">
                    @auth
                        <a href="{{ url('/dashboard') }}" class="text-base text-gray-700 underline">Accès au site</a>
                    @else
                        <a href="{{ route('login') }}" class="text-base text-gray-700 underline">Connexion</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-base text-gray-700 underline">Inscription</a>
                        @endif
                    @endauth
                </div>  
            @endif
            <div>
                
                <div class="justify-center sm:justify-start pt-0">
                    <div  class="flex items-center justify-center h-screen bg-fixed bg-center bg-cover custom-img3">
                        <p class="p-5 sm:text-2xl lg:text-2xl text-white bg-opacity-50 rounded-xl">Nous sommes dans une urgence écologique, ce n'est un secret pour personne : dérèglement climatique, destruction du vivant, pollutions des sols, de l'air et de l'eau, sans oublier les impacts sur notre santé. Notre espèce s'accroît et laisse de plus en plus son empreinte sur l'environnement. Il est nécessaire de changer nos habitudes, de consommer moins ou du moins mieux.
                        <br></br>Il est vain de chercher une ressource miracle qui nous sauvera. Il est nécessaire de moins produire. </p>
                    </div>
                </div>
                <div class="divhand justify-center sm:justify-start pt-0">
                    <div  class="flex items-center justify-center h-screen bg-fixed bg-center bg-cover custom-img4">
                        <p class="ombre p-5 sm:text-2xl lg:text-2xl bg-opacity-50 rounded-xl">Nous accumulons beaucoup de biens, souvent neufs, alors que nous ne les utilisons que très rarement. Nous devons changer notre modèle de société, modifier nos habitudes de consommation et vivre avec plus de sobriété. Au lieu d'acheter tous dans notre coin, pourquoi ne pas emprunter?
                        <br></br>L'emballage des objets neufs n'est que la partie visible de nos déchets. La majeure partie des déchets se crée durant la fabrication de l'objet. Les grandes enseignes de vêtements sont très polluantes, plus que les vols internationaux et le trafic maritime. Afin de vendre toujours moins cher, elles usent de pratiques immorales vis-à-vis des êtres humains. En 2018, l'ONG Zero Waste France lançait le défi "Rien de neuf". Le but étant de ne rien acheter de neuf dans l'année et de viser l'objectif du zéro déchet.</p>
                    </div>
                </div>
                <div class="justify-center sm:justify-start pt-0">
                    <div  class="flex items-center justify-center h-screen bg-fixed bg-center bg-cover custom-img5">
                        <p class="p-5 sm:text-2xl lg:text-2xl text-white bg-opacity-50 rounded-xl">Ce site est une approche pour respecter la règle des 5R : refuser (refuse), réduire (reduce), réutiliser (reuse), recycler (recycle) et composter (rot). <br></br>Après votre inscription sur ce site, vous pourrez effectuer un inventaire de vos biens partageables (livres, vêtements, bricolage, jeux ...). Par la suite, vous pourrez ajouter vos amis et ainsi voir ce que vous pouvez prêter ou emprunter.
                        <br></br>Astuce : Ce site vous permet de connaître un peu plus les goûts de vos amis et d'éviter les erreurs de cadeaux et les doublons.</p>
                    </div>
                </div>
            </div>
            
        </div>
    </body>
    <footer>
            <p class="text-center text-white">Copyright@2021</p>
            <p class="text-center text-white"><a href="/confidentialite">Politiques de confidentialité</a> | <a href="/cookies">Politiques des cookies</a><p>
    </footer>
</html>
