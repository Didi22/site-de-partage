<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Go Green') }}
        </h2>
    </x-slot>

    <div id="listArticles">
        <div class="tabarticles py-12">
        <div id="listeamis">
        </div>
            <table id="listeamisarticles" class="grid lg:grid-cols-4 sm:grid-cols-1 md:grid-cols-2">
            </table> 
        </div>
    </div>


<script type="text/javascript">
    let amisArray = <?php echo json_encode($amis); ?>;
    let objetsArray = <?php echo json_encode($objets); ?>;

    let divamis = document.querySelector('#listeamis');
    let tableau = document.querySelector('#listeamisarticles');

    let div1 = document.createElement('div');
    
    let tri = document.createElement('p');
    tri.innerHTML = "Trier par : ";

    let name = document.createElement('p');
    name.innerHTML = " Nom ";
    
    let slash1 = document.createElement('p');
    slash1.innerHTML = " / ";

    let category = document.createElement('p');
    category.innerHTML = " Catégorie ";
    
    let slash2 = document.createElement('p');
    slash2.innerHTML = " / ";

    let author = document.createElement('p');
    author.innerHTML = " Auteur ";

    let slash3 = document.createElement('p');
    slash3.innerHTML = " / ";

    let type = document.createElement('p');
    type.innerHTML = " Type de support ";

    amisArray.forEach((ami, index) => {
        let nom = document.createElement('p');
        nom.innerHTML = ami.name;
        nom.style.fontSize = '2em';
        nom.addEventListener('click', () => createArticlesListe(ami.ami_id));
        divamis.appendChild(nom);
    });

    function createArticlesListe(ami_id){
        let arrayObjets = objetsArray.filter(verifUserId);
       
        function verifUserId(objet){
            return objet.ami_id == ami_id;
        }

        div1.style.display = "flex";

        div1.appendChild(tri);
        div1.appendChild(name);
        div1.appendChild(slash1);
        div1.appendChild(category);
        div1.appendChild(slash2);
        div1.appendChild(author);
        div1.appendChild(slash3);
        div1.appendChild(type);
        divamis.appendChild(div1);

        function display(objet){
            let div2 = document.createElement('div');

            let image = document.createElement('img');
            image.src = objet.url;
            image.classList.add('transform', 'hover:scale-110', 'transition', 'duration-200');
            div2.appendChild(image);

            let categorie = document.createElement('p');
            categorie.innerHTML = objet.categorie;
            div2.appendChild(categorie);

            let nom = document.createElement('p');
            nom.innerHTML = objet.nom;
            div2.appendChild(nom);

            let description = document.createElement('p');
            description.innerHTML = objet.description;
            div2.appendChild(description);

            if(objet.categorie == 'livre'){
                let auteur = document.createElement('p');
                auteur.innerHTML = objet.auteur;
                div2.appendChild(auteur);

                let edition = document.createElement('p');
                edition.innerHTML = objet.edition;
                div2.appendChild(edition);

                let isbn = document.createElement('p');
                isbn.innerHTML = objet.ISBN;
                div2.appendChild(isbn);

                let support = document.createElement('p');
                support.innerHTML = objet.support;
                div2.appendChild(support);

                let date = document.createElement('p');
                date.innerHTML = objet.date;
                div2.appendChild(date);
            }

            if(objet.categorie == 'vetement'){
                let taille = document.createElement('p');
                taille.innerHTML = objet.taille;
                div2.appendChild(taille);
            }
            div2.style.padding = "2em";
            tableau.appendChild(div2);
        }
        
        name.addEventListener('click', (objet) => {
            let triNom = arrayObjets.sort((a, b) => a.nom.localeCompare(b.nom));
            tableau.innerHTML = "";
            triNom.forEach((objet, index) => {
               display(objet);
            });
        });

        category.addEventListener('click', () => {
            let triCategory = arrayObjets.sort((a, b) => a.categorie.localeCompare(b.categorie));  
            tableau.innerHTML = "";  
            triCategory.forEach((objet, index) => {
               display(objet);
            });     
        });

        author.addEventListener('click', () => {
            function verifCategorieLivre(objet){
                return objet.categorie == 'livre';
            }
            let triCategory = arrayObjets.filter(verifCategorieLivre).sort((a, b) => a.categorie.localeCompare(b.categorie));  
            tableau.innerHTML = "";  
            let triAuteur = triCategory.sort((a, b) => a.auteur.localeCompare(b.auteur));   
            triAuteur.forEach((objet, index) => {
                display(objet);
            });          
        });
       
        type.addEventListener('click', () => {
            function verifCategorieLivre(objet){
                return objet.categorie == 'livre';
            }
            let triCategory = arrayObjets.filter(verifCategorieLivre).sort((a, b) => a.categorie.localeCompare(b.categorie));  
            tableau.innerHTML = "";  
            let triSupport = triCategory.sort((a, b) => a.support.localeCompare(b.support));   
            triSupport.forEach((objet, index) => {
                display(objet);
            }); 
        });

        tableau.innerHTML = "";
        
        arrayObjets.forEach((objet, index) => {
            let div = document.createElement('div');
            
            let image = document.createElement('img');
            image.src = objet.url;
            image.classList.add('transform', 'hover:scale-110', 'transition', 'duration-200');
            div.appendChild(image);
            
            let categorie = document.createElement('p');
            categorie.innerHTML = objet.categorie;
            div.appendChild(categorie);

            let nom = document.createElement('p');
            nom.innerHTML = objet.nom;
            div.appendChild(nom);

            let description = document.createElement('p');
            description.innerHTML = objet.description;
            div.appendChild(description);

            if(objet.categorie == 'livre'){
                let auteur = document.createElement('p');
                auteur.innerHTML = objet.auteur;
                div.appendChild(auteur);

                let edition = document.createElement('p');
                edition.innerHTML = objet.edition;
                div.appendChild(edition);

                let isbn = document.createElement('p');
                isbn.innerHTML = objet.ISBN;
                div.appendChild(isbn);

                let support = document.createElement('p');
                support.innerHTML = objet.support;
                div.appendChild(support);

                let date = document.createElement('p');
                date.innerHTML = objet.date;
                div.appendChild(date);
            }

            if(objet.categorie == 'vetement'){
                let taille = document.createElement('p');
                taille.innerHTML = objet.taille;
                div.appendChild(taille);
            }
            div.style.padding = "2em";
            tableau.appendChild(div);
        });
    }
</script>

</x-app-layout>

