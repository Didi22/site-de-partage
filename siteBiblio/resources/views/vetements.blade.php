<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Go Green') }}
        </h2>
    </x-slot>

     
     @if(session()->has('success'))	
		<div class="alert alert-success" role="alert">
			<h4 class="alert-heading">{{ session()->get('success') }}</h4>
		</div>
	@endif
    <a href="javascript: switchForm();" id="lienachanger3">+</a> 
    <div class="ml-9 tabarticles py-12 flex flex-row">
        <div id="formcacher3"  style="display: none;">
            <div class="colorform p-6 border-b border-gray-200">
                <div class="article">
                    <h1>Ajouter un vêtement</h1>
                    <form method="POST" action="/vetement/nouveau" enctype="multipart/form-data" class="mt-5">
                    @csrf
                        <label>Nom</label>
                        <textarea rows="1" name="nom" placeholder="nom" class="form-control"></textarea>
                        <label>Taille</label>
                        <textarea rows="1" name="taille" placeholder="taille" class="form-control"></textarea>
                        <label>Description</label>
                        <textarea rows="1" name="description" placeholder="description" class="form-control"></textarea>
                        <div class="pt-4">
                            <label>Image</label>
                            <input type="file" name="url">
                        </div>
                        <div class="bouton">
                            <input type="submit" value="Ajouter" class="btn btn-primary mt-5"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="flex flex-column">
            <p> 
                <span class="underline">Trier par </span> : <a href="/dressing/?ordre=nom"> Nom </a> | <a href="/dressing/?ordre=taille"> Taille </a>   
            </p>

                <div class="grid lg:grid-cols-4 sm:grid-cols-1 md:grid-cols-2">
                @foreach ($objets as $objet)
                        @if($objet['categorie'] == 'vetement')
                        <div class="m-8 flex flex-column">
                                <p class="text-left"><a href="/vetement/delete/{{ $objet['id'] }}">x</a></p>
                                <p><img src="{{ asset($objet['url']) }}" class="transform hover:scale-110 transition duration-200 mt-8"></p>
                                <p><a href="/vetement/update/{{ $objet['id'] }}"> {{ $objet['nom'] }} </a></p> 
                                <p>{{$objet['description']}} </p>
                                <p>{{$objet['taille']}} </p>
                                <p>{{$objet['categorie']}} </p>
                        </div>
                        @endif
                    @endforeach 
                </div>   
            </div>
        </div>
</x-app-layout>

<script type="text/javascript">

    function switchForm()
    {
        // Montre ou cache le formulaire 
        let div = document.getElementById('formcacher3');
        if(div.style.display == 'none'){
            div.style.display = 'block';
        }else{
            div.style.display = 'none';
        }

        let lien = document.getElementById('lienachanger3');
        let contenu = "<-";
        let contenu2 = "+";
        if(lien.innerHTML == '+'){
            lien.innerHTML = contenu;
        }else{
            lien.innerHTML = contenu2;
        }
    }
</script>