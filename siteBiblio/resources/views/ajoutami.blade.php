<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Go Green') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="colorform p-6 border-b border-gray-200">
                    <h1>Demande(s) d'ami(s)</h1>
                    <form method="POST" action="/ami/ajout" class="mt-5">
                    @csrf  <!--  Blade insere un jeton/token dans un champ cache pour contrer les attaques   -->
                    <!-- token ressemble a : <input type="hidden" name="_token" value="sUTuW5dYrt2l2iPgAJ5EVXn5UwMjEGFLAZaH4jHz"> -->
                    @if(session()->has('success'))	
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">{{ session()->get('success') }}</h4>
                        </div>
                    @endif
                    @foreach($amis as $ami)
                        <div class="article">
                            <p> Cette personne souhaite vous ajouter dans sa liste d'amis : {{ $ami->name }}</p>
                            <p> Son adresse mail est : {{  $ami->email }} </p>
                            <input type="hidden" name="demande" value="{{  $ami->id }}"/>
                            <div class="bouton">
                                <button type="submit" name="statut" value="ami" class="btn btn-primary mt-5">Accepter la demande</button>
                                <button type="submit" name="non" value="non" class="btn btn-primary mt-5">Refuser la demande</button>
                            </div>
                        </div> 
                     @endforeach 
                    </form>   
                </div>   
            </div>
        </div>
    </div>
</x-app-layout>