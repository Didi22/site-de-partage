<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Go Green') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <h1>Supprimer le livre {{ $article->nom }}</h1>
                    <form method="POST" class="mt-5">
                        @csrf 
                        @method('delete')
                        @if(session('success'))
                        <p>{{ session('success') }}</p>
                        @endif
                        <label>Titre</label>
                        <input type="text" name="nom" value="{{ $article->nom }}" placeholder="Nom" class="form-control" />
                        <label>Auteur</label>
                        <input type="text" name="auteur" value="{{ $livre->auteur }}" placeholder="Auteur" class="form-control" />
                        <label>Description</label>
                        <input type="text" name="description" value="{{ $article->description }}" class="form-control" />
                        <label>Support</label>
                        <select name="categorie" size="1" id="articleChoix">
                            <option value="{{$livre->type_de_support}}" @if($livre->type_de_support == $livre->id) selected="selected" @endif>{{ $livre->type_de_support }}</option>   
                            <option value="livre">livre</option>
                            <option value="BD">BD</option>
                            <option value="manga">manga</option>
                            <option value="revue">revue</option>
                            <option value="autre">autre</option>
                            </select>
                            <div>
                            <label>Edition</label> 
                            <input type="text" name="edition" value="{{ $livre->edition }}" class="form-control" />
                            <label>ISBN</label>  
                            <input type="text" name="ISBN" value="{{ $livre->ISBN }}" class="form-control" />
                            <label>Date de publication</label>
                            <input type="text" name="date_de_publication" value="{{ $livre->date_de_publication->format('d m Y') }}" class="form-control" />
                            <label>Image</label>
                            <input type="file" name="url" value="{{ $article->url }}" class="form-control" />
                        </div>
                        <input type="submit" value="supprimer" class="btn btn-primary mt-5"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>