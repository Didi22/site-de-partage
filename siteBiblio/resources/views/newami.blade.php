<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Go Green') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="colorform p-6 border-b border-gray-200">
                  
                <div class="article">
                    <h1>Ajouter un ami</h1>
                    <form method="POST" action="/ami/recherche" class="mt-5">
                    @csrf
                    @if(session()->has('success'))	
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">{{ session()->get('success') }}</h4>
                        </div>
                    @endif
                    <tr>
                        <label>Pseudo</label>
                        <textarea rows="1" name="pseudo" placeholder="pseudo" class="form-control"></textarea>
                        <label>E-MAIL</label>
                        <textarea rows="1" name="email" placeholder="email" class="form-control"></textarea>
                        <div class="bouton">
                            <input type="submit" name="demande" value="Envoyer une demande" class="btn btn-primary mt-5"/>
                            <input type="submit"  name="suppression" value="Supprimer de la liste d'amis" class="btn btn-primary mt-5"/>
                        </div>
                    </form>
                    </div>
            </div>
        </div>
    </div>
</x-app-layout>