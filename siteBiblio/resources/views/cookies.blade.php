<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Go Green') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    Politique en matière de cookies
                </div>
                <div>
                    Comme la plupart des sites Web, des cookies sont utilisés. Un cookie est un fragment de texte qu'un serveur Web peut stocker sur le disque dur de l'utilisateur. Les cookies permettent aux sites Web de stocker des informations sur l'appareil de l'utilisateur et de récupérer ces informations ultérieurement. Il ne s'agit pas de programmes et ils ne peuvent donc effectuer aucune action sur l'ordinateur de l’utilisateur. Un site Web ne peut récupérer que les informations qu'il a placées sur l'ordinateur. Il n'a pas accès à d'autres fichiers cookies ou à toute autre information sur cet ordinateur.
                </div>
            </div>
        </div>
    </div>
</x-app-layout>